package com.test.cobaspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class CobaspringApplication {

	public static void main(String[] args) {
		SpringApplication.run(CobaspringApplication.class, args);
	}
}

@RestController
class GreetingController {

	@RequestMapping("/")
	String hello() {
		return "Hello, World !";
	}
}
