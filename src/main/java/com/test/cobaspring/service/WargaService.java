package com.test.cobaspring.service;

import com.test.cobaspring.model.Warga;

import java.util.List;
import java.util.Optional;

public interface WargaService {
    List<Warga> getAllWarga();
    Optional<Warga> findWargaById(Long id);
    Warga updateWargaById(Warga warga);
    Warga createWarga(Warga warga);
    void deleteWargaById(Long id);
}
