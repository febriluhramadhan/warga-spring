package com.test.cobaspring.controller;

import com.test.cobaspring.model.Warga;
import com.test.cobaspring.service.WargaService;
import com.test.cobaspring.utils.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping(value = "warga")
public class WargaController  {

    @Autowired
    WargaService wargaService;

    @PostMapping(value = "/create")
    ResponseEntity<Response> create (@RequestBody @Validated Warga warga){

        String nameofCurrMethod = new Throwable()
                .getStackTrace()[0]
                .getMethodName();
        Response response = new Response();
        response.setService(this.getClass().getName() + nameofCurrMethod);

        response.setData(wargaService.createWarga(warga));

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @PutMapping(value = "/update/{id}")
    ResponseEntity<Response> update (@PathVariable Long id, @RequestBody @Validated Warga warga)
    {
        String nameofCurrMethod = new Throwable()
                .getStackTrace()[0]
                .getMethodName();

        Response response = new Response();
        response.setService(this.getClass().getName() + nameofCurrMethod);

        Optional<Warga> wg = wargaService.findWargaById(id);
        if(!wg.isPresent())
        {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .contentType(MediaType.APPLICATION_JSON)
                    .build();
        }
        warga.setId(id);
        response.setData(wargaService.updateWargaById(warga));

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @GetMapping(value = "/get/{id}")
    public ResponseEntity<Response> getById (@PathVariable Long id)
    {
        String nameofCurrMethod = new Throwable()
                .getStackTrace()[0]
                .getMethodName();

        Response response = new Response();
        response.setService(this.getClass().getName() + nameofCurrMethod);

        Optional<Warga> wg = wargaService.findWargaById(id);
        if(!wg.isPresent())
        {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .contentType(MediaType.APPLICATION_JSON)
                    .build();
        }
        response.setData(wg);

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @GetMapping(value = "/getall")
    ResponseEntity<Response> findAll ()
    {
        String nameofCurrMethod = new Throwable()
                .getStackTrace()[0]
                .getMethodName();

        Response response = new Response();
        response.setService(this.getClass().getName() + nameofCurrMethod);

        response.setData(wargaService.getAllWarga());

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }

    @DeleteMapping(value = "/delete/{id}")
    ResponseEntity<Response> deleteById (@PathVariable Long id)
    {
        String nameofCurrMethod = new Throwable()
                .getStackTrace()[0]
                .getMethodName();

        Response response = new Response();
        response.setService(this.getClass().getName() + nameofCurrMethod);

        Optional<Warga> wg = wargaService.findWargaById(id);
        if(!wg.isPresent())
        {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .contentType(MediaType.APPLICATION_JSON)
                    .build();
        }

        response.setData(wg);

        wargaService.deleteWargaById(id);

        return  ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(response);
    }
}
