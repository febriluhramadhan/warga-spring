package com.test.cobaspring.repository;

import com.test.cobaspring.model.Warga;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WargaRepository extends JpaRepository<Warga,Long> {
}
