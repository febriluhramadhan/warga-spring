package com.test.cobaspring.implement;

import com.test.cobaspring.model.Warga;
import com.test.cobaspring.repository.WargaRepository;
import com.test.cobaspring.service.WargaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WargaServiceImpl implements WargaService {

    @Autowired
    WargaRepository wargaRepository;

    @Override
    public List<Warga> getAllWarga() {
        return wargaRepository.findAll();
    }

    @Override
    public Optional<Warga> findWargaById(Long id) {
        return wargaRepository.findById(id);
    }

    @Override
    public Warga updateWargaById(Warga warga) {
        return wargaRepository.save(warga);
    }

    @Override
    public Warga createWarga(Warga warga) {
        return wargaRepository.save(warga);
    }

    @Override
    public void deleteWargaById(Long id) {
        wargaRepository.deleteById(id);
    }
}
